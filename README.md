# Activity

In `course_bookings` collection of `aggregation_db` database, develop the code for the following instructions:

- Count the completed courses of student `S013`.
- Show students who are not yet completed in their course, without showing the course IDs. 
- Sort the `courseId` in descending order while `studentId` in ascending order.

Each bullet must have their own aggregation pipeline code.  
Save the code in a file (i.e., `README.md`) and push it to your GitLab repository (i.e., `s19-a1`).

## References

- [$match (aggregation)](https://docs.mongodb.com/manual/reference/operator/aggregation/match/index.html)
- [$group (aggregation)](https://docs.mongodb.com/manual/reference/operator/aggregation/group/index.html)
- [$project (aggregation)](https://docs.mongodb.com/manual/reference/operator/aggregation/project/index.html)
- [$sort (aggregation)](https://docs.mongodb.com/manual/reference/operator/aggregation/sort/index.html)

## Solution

### Count the completed courses of student `S013`.

```
db.course_bookings.aggregate([

	{
		$match: {
			$and: [{
				"isCompleted": true
			}, {
				"studentId": "S013"
			}]
		}
	}

	{
		$group: {
			_id: null,
			coursesCompletedByThisStudent: {
				$sum: 1
			}
		}
	}

]);
```

### Show students who are not yet completed in their course, without showing the course IDs.

```
db.course_bookings.aggregate([
	{
		$match: {
			"isCompleted": false
		}
	}

	{
		$project: {
			"courseId": 0
		}
	}

]);
```

### Sort the `courseId` in descending order while `studentId` in ascending order.

```
db.course_bookings.aggregate([
	{ $sort: { courseId: -1, studentId: 1 }}
])
```